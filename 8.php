<?php

$radio = 0;
$longitud = 0;
$area = 0;
$volumen = 0;

if (isset($_GET['enviar'])) {
    $radio = $_GET['radio'];

    $longitud = 2 * pi() * $radio;
    $area = pi() * $radio ** 2;
    $volumen = 4 / 3 * pi() * $radio ** 3;

?>
    <p>El radio introducido es: <?= round($radio, 2) ?> </p>
    <p>La longitud de la circuferencia es: <?= round($longitud, 2) ?> </p>
    <p>El área del círculo es: <?= round($area, 2) ?> </p>
    <p>El volumen de la esfera es: <?= round($volumen, 2) ?> </p>

<?php
}
?>