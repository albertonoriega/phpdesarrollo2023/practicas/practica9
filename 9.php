<?php

$nota = 0;


if (isset($_GET['enviar'])) {
    $nota = $_GET['nota'];

    if ($nota >= 0 && $nota < 3) {
        $salida = "muy deficiente";
    } elseif ($nota < 5) {
        $salida = "suspenso";
    } elseif ($nota < 6) {
        $salida = "aprobado";
    } elseif ($nota < 7) {
        $salida = "bien";
    } elseif ($nota < 9) {
        $salida = "notable";
    } elseif ($nota < 10) {
        $salida = "sobresaliente";
    } else {
        $salida = "Nota introducida no válida";
    }
?>

    <p> <?= $salida ?></p>
<?php
}
?>