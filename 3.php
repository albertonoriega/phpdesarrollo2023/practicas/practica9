<?php
$caracter = "";
$vocales = ['a', 'e', 'i', 'o', 'u'];
// Por defecto consideramos que no es una vocal
$salida = "El carácter introducido no es una vocal";

if (isset($_GET['enviar'])) {
    $caracter = $_GET['caracter'];

    foreach ($vocales as $valor) {
        if (strtolower($caracter) == $valor) {
            $salida = "El carácter introducido es una vocal";
        }
    }
}

echo $salida;
