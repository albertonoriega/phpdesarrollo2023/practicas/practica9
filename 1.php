<?php

$nombre = "";
$color = "";
$alto = 0;
$peso = 0;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            border: 1px solid black;
            border-collapse: collapse;
        }

        td {
            border: 1px solid black;
            width: 150px;
            padding: 10px;
            text-align: center;
        }
    </style>
</head>

<body>
    <?php
    if (isset($_GET['enviar'])) {
        $nombre = $_GET['nombre'];
        $color = $_GET['color'];
        $alto = $_GET['alto'];
        $peso = $_GET['peso'];

    ?>
        <table>
            <tr>
                <td>Nombre</td>
                <td> <?= $nombre ?></td>
            </tr>
            <tr>
                <td>Color</td>
                <td><?= $color ?></td>
            </tr>
            <tr>
                <td>Alto</td>
                <td><?= $alto ?></td>
            </tr>
            <tr>
                <td>Peso</td>
                <td><?= $peso ?></td>
            </tr>
        </table>
    <?php
    }
    ?>
</body>

</html>