    <?php
    $numero1 = 0;
    $numero2 = 0;
    $resultado = 0;
    if (isset($_GET["suma"])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "La suma es: " .  $numero1 + $numero2;
        echo $resultado;
    } elseif (isset($_GET['resta'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "La resta es: " . $numero1 - $numero2;
        echo $resultado;
    } elseif (isset($_GET['producto'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "El producto es: " . $numero1 * $numero2;
        echo $resultado;
    } elseif (isset($_GET['division'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "La división es: " . $numero1 / $numero2;
        echo $resultado;
    } elseif (isset($_GET['raizCuadrada'])) {
        $numero1 = $_GET['numero1'];

        $resultado = "La raiz cuadrada del número 1: " . $numero1 ** (1 / 2);
        echo $resultado;
    } else if (isset($_GET['potencia'])) {
        $numero1 = $_GET['numero1'];
        $numero2 = $_GET['numero2'];

        $resultado = "El primer número elevado al segundo es: " . $numero1 ** $numero2;
        echo $resultado;
    }
